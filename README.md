Développement Web 2 
================================

Répertoire de vélos volés
--------------------------------------------------------
**Dépôt Bitbucket :** [Lien](https://bitbucket.org/walensis/velma.git)

## Team
Soufiane BENNANI, Hamza EL MEKNASSI.

## Synopsis
L’idée du projet est la création d'un site permettant la gestion de titres de propriétés de vélos. Il
s’agit de créer un site où les utilisateurs pourront :

- S’inscrire.
- Se déclarer comme propriétaires de matériel sportif.
- Déclarer leur matériel comme étant volé.
- Consulter la liste du matériel déclaré comme volé.

Les utilisateurs inscrits au site pourront ainsi :

- Déposer sur ce site des documents témoignant de l’achat de leur matériel.
- Déposer des photos de leur matériel.

Tous les utilisateurs (inscrits ou non) pourront consulter la liste du matériel déclaré comme volé.
L’idée est que, s’il soupçonne qu’un objet (dont ils ont connaissance dans la vie « réelle », par
exemple un objet vendu en magasin) est volé, alors ils pourront le rechercher dans la liste du
matériel déclaré comme volé et, si cet objet y figure, pouvoir contacter (envoyer donc un courriel
par un formulaire) la personne qui s’en est déclarée propriétaire.

Le site web est réalisé avec le framework PHP Symfony 3.4.

## Usage
**Prérequis :** PHP 7+, Composer, Git

- `clone https://bitbucket.org/walensis/velma.git`
- `composer update`
- `git checkout develop` (ou la branche la plus à jour)
- `php bin/console server:start`
- Visiter l'adresse sur laquelle écoute le serveur PHP, par défaut `http://localhost:8000/`