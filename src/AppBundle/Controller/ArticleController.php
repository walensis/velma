<?php

namespace AppBundle\Controller;

use AppBundle\AppBundle;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\FileType;
use AppBundle\Entity\Velo;
use Symfony\Component\HttpFoundation\File\File;

class ArticleController extends Controller
{

    /**
     * @Route("/article/{id}", name="view_article", requirements={"id"="\d+"})
     */
    public function viewAction($id)
    {
         $article = $this->getDoctrine()
            ->getRepository(Velo::class)
            ->find($id);

        return $this->render('AppBundle:Article:view.html.twig', array(
            'article' => $article
        ));
    }

    /**
     * @Route("/article/edit/{id}", name="edit_article", requirements={"id"="\d+"}))
     */
    public function editAction($id, Request $request)
    {
        $article = $this->getDoctrine()
                        ->getRepository(Velo::class)
                        ->find($id);

        if (!$article) {
            throw $this->createNotFoundException(
                'No article found for id '.$id
            );
        }
        $relative_to_visual = $article->getVisual();
        $article->setVisual(
            new File($this->getParameter('img_directory').'/'.$article->getVisual())
        );

        if ($article->getRobbedAt()==NULL || $article->getRobbedLocation()==NULL) {
            $builder = $this->createFormBuilder($article)
                ->add('isRobbed', CheckboxType::class, array(
                    'label' => 'Volé',
                    'mapped' => false,
                    'data' => false,
                    'required' => false
                ));
        } else {
            $builder = $this->createFormBuilder($article)
                ->add('isRobbed', CheckboxType::class, array(
                    'label' => 'Volé',
                    'mapped' => false,
                    'data' => true,
                    'required' => false
                ));
        }

        $form = $builder
            ->add('visual', FileType::class, array(
                'label' => 'Image',
                'required' => false,
                'empty_data' => false
                ))
            ->add('title', TextType::class, array('required' => true))
            ->add('brand', TextType::class, array('label' => 'Marque', 'required' => true))
            ->add('model', TextType::class, array('label' => 'Modèle', 'required' => true))
            ->add('type', TextType::class, array('label' => 'Type de vélo'))
            ->add('boughtAt', DateType::class, array('label' => 'Date d\'achat', 'required' => true))
            ->add('robbedAt', DateType::class, array('label' => 'Date du vol'))
            ->add('robbedLocation', TextType::class, array('label' => 'Lieu du vol', 'required' => false))
            ->add('description', TextareaType::class, array('label' => 'Description', 'required' => true))
            ->add('save', SubmitType::class, array('label' => 'Mettre à jour'))
            ->getForm();

        // PROCESSING POST DATA
        $form->handleRequest($request);

        if ($form->isSubmitted()) {
            $entityManager = $this->getDoctrine()->getManager();
            // $form->getData() holds the submitted values
            // but, the original `$task` variable has also been updated
            $article_update = $form->getData();

            if ($article_update->getVisual() == false)
                $article_update->setVisual($relative_to_visual);

            if (!$form->get('isRobbed')->getData()) {
                $article->setRobbedAt(NULL);
                $article->setRobbedLocation(NULL);
            } else { // In case isRobbed is true but not location AND datetime both set
                if ($article_update->getRobbedAt()==NULL || $article_update->getRobbedLocation()==NULL) {
                    $article_update->setRobbedAt(NULL); $article_update->setRobbedLocation(NULL);

                    // Still persist the rest of the data so it won't be lost
                    $entityManager->persist($article_update);
                    $entityManager->flush();

                    return $this->redirectToRoute('edit_article', array(
                        'id' => $id,
                        'flash_msg' => "Vous devez renseigner la date et lieu du vol si vous souhaitez déclarer votre matériel comme volé."));
                }
            }

            // ... perform some action, such as saving the task to the database
            // for example, if Task is a Doctrine entity, save it!

            $entityManager->persist($article_update);
            $entityManager->flush();

            return $this->redirectToRoute('view_article', array('id' => $id));
        }

        // RETURNING GET VIEW
        return $this->render('AppBundle:Article:edit.html.twig', array(
            'form' => $form->createView(),
            'art_visual' => $relative_to_visual
        ));
    }

    /**
     * @Route("/article/new", name="create_article")
     */
    public function createAction(Request $request)
    {
        $article = new Velo();

        if ($request->query->get('title') !== NULL || $request->query->get('title') !== '')
            $title = $request->query->get('title');
        else
            $title = '';

        $form = $this->createFormBuilder($article)
            ->add('visual', FileType::class, array('label' => 'Image'))
            ->add('title', TextType::class, array(
                'label' => false,
                'required' => true,
                'attr' => array(
                    'placeholder' => 'Titre'
                ),
                'data' => $title
            ))
            ->add('brand', TextType::class, array('label' => 'Marque', 'required' => true))
            ->add('model', TextType::class, array('label' => 'Modèle', 'required' => true))
            ->add('type', TextType::class, array('label' => 'Type de vélo'))
            ->add('boughtAt', DateType::class, array('label' => 'Date d\'achat', 'required' => true))
            ->add('isRobbed', CheckboxType::class, array(
                'label' => 'Volé',
                'mapped' => false,
                'data' => false,
                'required' => false
            ))
            ->add('robbedAt', DateType::class, array('label' => 'Date du vol'))
            ->add('robbedLocation', TextType::class, array('label' => 'Lieu du vol', 'required' => false))
            ->add('description', TextareaType::class, array('label' => 'Description', 'required' => true))
            ->add('save', SubmitType::class, array('label' => 'Créer'))
            ->getForm();

        // PROCESSING FORM DATA FROM POST REQUEST
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $entityManager = $this->getDoctrine()->getManager();
            // $form->getData() holds the submitted values
            // but, the original `$task` variable has also been updated
            $article = $form->getData();
            $article->setCreatedAt(new \DateTime());

            // PROCESSING IMAGE FILE
            /** @var \Symfony\Component\HttpFoundation\File\UploadedFile $img */
            $img = $article->getVisual();
            $imgName = $this->generateUniqueFileName().'.'.$img->guessExtension();
            $img->move(
                $this->getParameter('img_directory'),
                $imgName
            );
            $article->setVisual($imgName);

            // LINKING TO USER

            $article->setOwner($this->getUser());

            if (!$form->get('isRobbed')->getData()) {
                $article->setRobbedAt(NULL);
                $article->setRobbedLocation(NULL);
            } else { // In case isRobbed is true but location AND datetime not both set
                if ($article->getRobbedAt()==NULL || $article->getRobbedLocation()==NULL) {
                    $article->setRobbedAt(NULL); $article->setRobbedLocation(NULL);

                    // Still persist the rest of the data so it won't be lost
                    $entityManager->persist($article);
                    $entityManager->flush();

                    return $this->redirectToRoute('edit_article', array(
                        'id' => $article->getId(),
                        'flash_msg' => "Vous devez renseigner la date et lieu du vol si vous souhaitez déclarer votre matériel comme volé."));
                }
            }


            // ... perform some action, such as saving the task to the database
            // for example, if Task is a Doctrine entity, save it!
            $entityManager->persist($article);
            $entityManager->flush();

            return $this->redirectToRoute('view_article', array('id' => $article->getId()));
        }

        if (!$article) {
            throw $this->createNotFoundException(
                'No article created'
            );
        }

        //RETURNING EMPTY FORM FOR GET REQUEST
        return $this->render('AppBundle:Article:create.html.twig', array(
            'form' => $form->createView()
    ));
    }

    /**
     * @Route("/article/{id}/found", requirements={"id"="\d+"}))
     */
    public function foundAction($id)
    {
        $article = $this->getDoctrine()
            ->getRepository(Velo::class)
            ->find($id);

        if (!$article) {
            throw $this->createNotFoundException(
                'No article found for id '.$id
            );
        }

        $form = $this->createFormBuilder()
            ->add('fullname', TextType::class, array('label' => 'Nom', 'mapped' => false))
            ->add('email', TextType::class, array('label' => 'Votre e-mail', 'mapped' => false))
            ->add('subject', TextType::class, array('label' => 'Objet', 'mapped' => false))
            ->add('message', TextareaType::class, array('label' => 'Message', 'mapped' => false))
            ->add('save', SubmitType::class, array('label' => 'Envoyer'))
            ->getForm();


        return $this->render('AppBundle:Article:found.html.twig', array(
            'article' => $article,
            'form' => $form->createView()
        ));
    }

    /**
     * @return string
     */
    private function generateUniqueFileName()
    {
        // md5() reduces the similarity of the file names generated by
        // uniqid(), which is based on timestamps
        return md5(uniqid());
    }

}
