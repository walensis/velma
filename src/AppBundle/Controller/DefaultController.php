<?php

namespace AppBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use AppBundle\Entity\Velo;
use AppBundle\Repository\VeloRepository;

class DefaultController extends Controller
{
    /**
     * @Route("/{sort}", name="homepage", requirements={"sort"="(chrono)|(alpha)"})
     */
    public function indexAction(Request $request, $sort = 'chrono')
    {
        $articles = $this->getDoctrine()
            ->getRepository(Velo::class)
            ->findAll($sort);

        $form = $this->createFormBuilder()
            ->add('search', TextType::class, array(
                'mapped' => false,
                'required' => true,
                'label' => false))
            ->getForm();

        //Generating date intervals
        $now = new \DateTime();
        foreach ($articles as $article) {
            $interval = $now->diff($article->getRobbedAt());
            if (!empty($interval->y)) {
                if (empty($interval->m))
                    $article->interval = $interval->format('%y ans');
                else
                    $article->interval = $interval->format('%y ans et %m mois');
            }
            else if (!empty($interval->m)) {
                if (empty($interval->d))
                    $article->interval = $interval->format('%m mois');
                else
                    $article->interval = $interval->format('%m mois et %d jours');
            }
            else if (!empty($interval->d))
                $article->interval = $interval->format('%d jours');
            else if (!empty($interval->h)) {
                if (empty($interval->i))
                    $article->interval = $interval->format('%h heures');
                else
                    $article->interval = $interval->format('%h heures et %i minutes');
            }
            else if (!empty($interval->i))
                $article->interval = $interval->format('%i minutes');
            else
                $article->interval = $interval->format('%s secondes');
        }

        return $this->render('default/index.html.twig', [
            'articles' => $articles,
            'sort' => $sort,
            'form' => $form->createView()
        ]);
    }

    /**
     * @Route("/contact", name="contact")
     */
    public function contactAction(Request $request)
    {
        $form = $this->createFormBuilder(null, array('action' => '/search', 'method' => 'POST'))
            ->add('fullname', TextType::class, array('label' => 'Nom', 'mapped' => false, 'required' => true))
            ->add('email', TextType::class, array('label' => 'Votre e-mail', 'mapped' => false, 'required' => true))
            ->add('subject', ChoiceType::class, array(
                'choices'  => array(
                    'Suggestion de fonctionnalité' => 'feature',
                    'Litige important avec un utilisateur' => 'user_issue',
                    'Demande de renseignements' => 'info_request',
                    'Business' => 'business'
                ),
                'label' => 'Objet',
                'mapped' => false,
                'required' => true))
            ->add('message', TextareaType::class, array('label' => 'Message', 'mapped' => false, 'required' => true))
            ->add('save', SubmitType::class, array('label' => 'Envoyer'))
            ->getForm();

        // replace this example code with whatever you need
        return $this->render('default/contact.html.twig', [
            'form' => $form->createView()
        ]);
    }

    /**
     * @Route("/search/{sort}", name="search", methods={"POST"}, requirements={"sort"="(chrono)|(alpha)"})
     */
    public function searchAction(Request $request, $sort = 'chrono')
    {
        $vrepo = $this->getDoctrine()->getRepository(Velo::class);
        $form = $this->createFormBuilder()
            ->add('search', TextType::class, array(
                'mapped' => false,
                'required' => true,
                'label' => false))
            ->getForm();


        // PROCESSING POST DATA
        $form->handleRequest($request);

        if ($form->isSubmitted()) {
            $query = $form->get('search')->getData();
            $articles = $vrepo->findByString($query);
        } else {
            die("FORM NOT SENT");
            //REDIRECT TO HOMEPAGE
        }

        //Generating date intervals
        $now = new \DateTime();
        foreach ($articles as $article) {
            $interval = $now->diff($article->getRobbedAt());
            if (!empty($interval->y)) {
                if (empty($interval->m))
                    $article->interval = $interval->format('%y ans');
                else
                    $article->interval = $interval->format('%y ans et %m mois');
            }
            else if (!empty($interval->m)) {
                if (empty($interval->d))
                    $article->interval = $interval->format('%m mois');
                else
                    $article->interval = $interval->format('%m mois et %d jours');
            }
            else if (!empty($interval->d))
                $article->interval = $interval->format('%d jours');
            else if (!empty($interval->h)) {
                if (empty($interval->i))
                    $article->interval = $interval->format('%h heures');
                else
                    $article->interval = $interval->format('%h heures et %i minutes');
            }
            else if (!empty($interval->i))
                $article->interval = $interval->format('%i minutes');
            else
                $article->interval = $interval->format('%s secondes');
        }


        return $this->render('default/index.html.twig', [
            'articles' => $articles,
            'sort' => $sort,
            'form' => $form->createView()
        ]);
    }
}
