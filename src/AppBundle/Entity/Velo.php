<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\ORM\Mapping\ManyToOne;
use Doctrine\ORM\Mapping\JoinColumn;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * Velo
 *
 * @ORM\Table(name="velo")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\VeloRepository")
 */
class Velo
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="title", type="string", length=255)
     */
    private $title;

    /**
     * @var string
     *
     * @ORM\Column(name="brand", type="string", length=255, nullable=false)
     */
    private $brand;

    /**
     * @var string
     *
     * @ORM\Column(name="model", type="string", length=255, nullable=true)
     */
    private $model;

    /**
     * @var string
     *
     * @ORM\Column(name="type", type="string", length=255, nullable=true)
     */
    private $type;

    /**
     * @var string
     *
     * @ORM\Column(name="description", type="text", length=1020, nullable=true)
     */
    private $description;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="boughtAt", type="datetime")
     */
    private $boughtAt;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="createdAt", type="datetime")
     */
    private $createdAt;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="robbedAt", type="datetime", nullable=true)
     */
    private $robbedAt;

    /**
     * @var \string
     *
     * @ORM\Column(name="robbedLocation", type="string", nullable=true)
     */
    private $robbedLocation;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="foundAt", type="datetime", nullable=true)
     */
    private $foundAt;

    /**
     * Many Bikes have One User.
     * @ManyToOne(targetEntity="User", inversedBy="bikes")
     * @JoinColumn(name="user_id", referencedColumnName="id")
     */
    private $owner;

    /**
     * @ORM\Column(type="string")
     *
     * @Assert\File(mimeTypes={ "image/png", "image/jpeg", "image/gif" })
     */
    private $visual;


    /***********
     * METHODS *
     ***********/

    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set title
     *
     * @param string $title
     *
     * @return Velo
     */
    public function setTitle($title)
    {
        $this->title = $title;

        return $this;
    }

    /**
     * Get title
     *
     * @return string
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * Set brand
     *
     * @param string $brand
     *
     * @return Velo
     */
    public function setBrand($brand)
    {
        $this->brand = $brand;

        return $this;
    }

    /**
     * Get brand
     *
     * @return string
     */
    public function getBrand()
    {
        return $this->brand;
    }

    /**
     * Set model
     *
     * @param string $model
     *
     * @return Velo
     */
    public function setModel($model)
    {
        $this->model = $model;

        return $this;
    }

    /**
     * Get model
     *
     * @return string
     */
    public function getModel()
    {
        return $this->model;
    }

    /**
     * Set type
     *
     * @param string $type
     *
     * @return Velo
     */
    public function setType($type)
    {
        $this->type = $type;

        return $this;
    }

    /**
     * Get type
     *
     * @return string
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * Set description
     *
     * @param string $description
     *
     * @return Velo
     */
    public function setDescription($description)
    {
        $this->description = $description;

        return $this;
    }

    /**
     * Get description
     *
     * @return string
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * Set boughtAt
     *
     * @param \DateTime $boughtAt
     *
     * @return Velo
     */
    public function setBoughtAt($boughtAt)
    {
        $this->boughtAt = $boughtAt;

        return $this;
    }

    /**
     * Get boughtAt
     *
     * @return \DateTime
     */
    public function getBoughtAt()
    {
        return $this->boughtAt;
    }

    /**
     * Set createdAt
     *
     * @param \DateTime $createdAt
     *
     * @return Velo
     */
    public function setCreatedAt($createdAt)
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    /**
     * Get createdAt
     *
     * @return \DateTime
     */
    public function getCreatedAt()
    {
        return $this->createdAt;
    }

    /**
     * Set robbedAt
     *
     * @param \DateTime $robbedAt
     *
     * @return Velo
     */
    public function setRobbedAt($robbedAt)
    {
        $this->robbedAt = $robbedAt;

        return $this;
    }

    /**
     * Get robbedAt
     *
     * @return \DateTime
     */
    public function getRobbedAt()
    {
        return $this->robbedAt;
    }

    /**
     * Set robbedLocation
     *
     * @param string $robbedLocation
     *
     * @return Velo
     */
    public function setRobbedLocation($robbedLocation)
    {
        $this->robbedLocation = $robbedLocation;

        return $this;
    }

    /**
     * Get robbedLocation
     *
     * @return string
     */
    public function getRobbedLocation()
    {
        return $this->robbedLocation;
    }

    /**
     * Set foundAt
     *
     * @param \DateTime $foundAt
     *
     * @return Velo
     */
    public function setFoundAt($foundAt)
    {
        $this->foundAt = $foundAt;

        return $this;
    }

    /**
     * Get foundAt
     *
     * @return \DateTime
     */
    public function getFoundAt()
    {
        return $this->foundAt;
    }

    /**
     * Get owner
     *
     * @return \AppBundle\Entity\User
     */
    public function getOwner() {
        return $this->owner;
    }

    /**
     * Set owner
     *
     * @param \AppBundle\Entity\User $owner_update
     *
     * @return Velo
     */
    public function setOwner(User $owner_update = null) {
        $this->owner = $owner_update;

        return $this;
    }

    /**
     * Get visual
     *
     * @return string
     */
    public function getVisual()
    {
        return $this->visual;
    }

    /**
     * Set visual
     *
     * @param string $visual
     *
     * @return Velo
     */
    public function setVisual($visual)
    {
        $this->visual = $visual;

        return $this;
    }
}

